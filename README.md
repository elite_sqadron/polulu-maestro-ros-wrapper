ROS2 Wrapper for [maestro.py](https://github.com/FRC4564/Maestro/blob/master/maestro.py). It allows for communication with polulu maestro controler using ROS2 topics.

## Install
1. Install prerequisites
   1. [pyserial](https://pypi.org/project/pyserial/)
   2. [ROS 2 Maestro pynode](https://gitlab.com/elite_sqadron/polulu-maestro-pynode)
2. Navigate to (or create) src subfolder of your ROS2 workspace (`cd ros2_ws/src`)
3. `git pull https://gitlab.com/elite_sqadron/polulu-maestro-pynode.git`
4. navigate one level up (`cd ../`)
5. build package with colcon (`colcon build --packages-select servo_controller_interfaces`)
