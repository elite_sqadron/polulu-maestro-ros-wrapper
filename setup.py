from setuptools import setup

package_name = 'polulu_maestro_ros_py'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Jakub Mazur',
    maintainer_email='kubam0906@gmail.com',
    description='Package wraps up https://github.com/FRC4564/Maestro/blob/master/maestro.py class in ROS 2 wrapper that comunicates with outside world using topics. ',
    license='MIT License',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
		'maestro_wrapper = polulu_maestro_ros_py.MaestroRosWrapper:main',
		'maestro_test_pub = polulu_maestro_ros_py.MaestroTestPub:main',
        ],
    },
)
