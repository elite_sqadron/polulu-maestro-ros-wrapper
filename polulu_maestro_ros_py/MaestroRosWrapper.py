import rclpy

from cmath import pi
from rclpy.node import Node


from servo_controller_interfaces.msg import ServoTarget, CurrentPositionsFeedback

from polulu_maestro_ros_py.MaestroController import Controller

class ServoCalibrations: # TODO - make calibrations settable as ROS parameters
    #Values in nanoseconds
    zero = 1496
    plus_90 = 2496
    #values in radians:
    range_up = pi/2
    range_down = -pi/2




class MaestroRosWrapper(Node):
    def __init__(self):
        super().__init__('maestro_ros_wrapper')
        self.target_subscription = self.create_subscription(
            ServoTarget,
            'new_servo_target',
            self.target_callback,
            3)

        self.current_pos_publisher = self.create_publisher(
            CurrentPositionsFeedback,
            'current_positions_feedback',
            3)

        timer_period = 0.003  # seconds TODO - settable by params
        self.timer = self.create_timer(
            timer_period,
            self.pos_publisher_timer_callback)

        self.maestro = Controller()



    def rad2qns(self, rad: float, serv_cal: ServoCalibrations = ServoCalibrations()) -> int:
        # 2496 * 4: -90, -PI/2
        # 496  * 4: 90, PI/2
        # 1496 * 4: 0, 0
        temp = (((rad * (serv_cal.plus_90 - serv_cal.zero))/(pi/2)) + serv_cal.zero)*4

        if temp > 2496*4:
            temp = 2496*4
            self.get_logger().warning('recieved angle is too big, cutting to max possible angle!', skip_first=False)
        if temp <  496*4:
            temp =  496*4
            self.get_logger().warning('recieved angle is too small, cutting to min possible angle!', skip_first=False)

        return int(temp)

    def pos_publisher_timer_callback(self):
        msg = CurrentPositionsFeedback()
        msg.current_ang = [self.maestro.getPosition(i) * 1.0 for i in range(24)]

        self.current_pos_publisher.publish(msg)

    def target_callback(self, msg):
        self.get_logger().info(f'{msg.channel}: {msg.target_ang}')
            
        self.maestro.setTarget(msg.channel, self.rad2qns(msg.target_ang))
        self.maestro.setSpeed(msg.channel, int(msg.speed))# TODO - recalculate from radians/s to quartermicroseconds/s
        self.maestro.setAccel(msg.channel, int(msg.acceleration))# TODO - recalculate (similar as above)

def main(args=None):
    rclpy.init(args=args)

    maestro_ros_wrapper = MaestroRosWrapper()

    rclpy.spin(maestro_ros_wrapper)

    maestro_ros_wrapper.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
